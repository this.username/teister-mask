const Task = require('../models/Task');

module.exports = {
	index: (req, res) => {
		let openTasks = [];
		let inProgressTasks = [];
		let finishedTasks = [];

        Task.find().then(tasks => {
        	for(let i = 0; i < tasks.length; i++){
        		let currentTask = tasks[i];
        		if (currentTask.status == "Open"){
        			openTasks.push(currentTask);
				} else if (currentTask.status == "In Progress"){
        			inProgressTasks.push(currentTask);
				} else{
					finishedTasks.push(currentTask);
				}
			};
		});

        return res.render("task/index", {"openTasks": openTasks, "inProgressTasks": inProgressTasks, "finishedTasks": finishedTasks});
    },
	createGet: (req, res) => {
		res.render("task/create");
	},
	createPost: (req, res) => {
		let task = req.body;
		Task.create(task).then(task => res.redirect("/"));
	},
	editGet: (req, res) => {
		let taskId = req.params.id;
		Task.findById(taskId).then(task => {
			res.render("task/edit", task);
		})
	},
	editPost: (req, res) => {
        let taskId = req.params.id;
        let currentTask = req.body;
        Task.findByIdAndUpdate(taskId, currentTask).then(task => {
        	return res.redirect("/");
		});
	}
};